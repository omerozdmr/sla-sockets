package eu.atos.sla.core;

import eu.project.rapid.common.RapidMessages;
import eu.project.rapid.common.RapidUtils;
import org.apache.log4j.Logger;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;


public class ThreadPoolServer implements Runnable {

    private final static Logger logger = Logger.getLogger(ThreadPoolServer.class);

    private int serverPort;
    private ServerSocket serverSocket = null;

    static List<String> vmmIPList = new ArrayList<>();

    private boolean isStopped = false;
    private Thread runningThread = null;
    private ExecutorService threadPool = null;

    ThreadPoolServer(int serverPort, int threadNumber,
                     String animationServerIp, int animationServerPort) {
        this.serverPort = serverPort;
        RapidUtils.sendAnimationMsg(animationServerIp, animationServerPort, String.valueOf(RapidMessages.AnimationMsg.SLAM_REGISTER_DS));
        threadPool = Executors.newFixedThreadPool(threadNumber);
    }

    public void run() {
        synchronized (this) {
            this.runningThread = Thread.currentThread();
        }
        openServerSocket();
        while (!isStopped()) {
            Socket clientSocket;
            try {
                clientSocket = this.serverSocket.accept();
            } catch (IOException e) {
                if (isStopped()) {
                    logger.error("Server Stopped.", e);
                    break;
                }
                throw new RuntimeException("Error accepting client connection", e);
            }
            this.threadPool.execute(new WorkerRunnable(clientSocket));
        }
        this.threadPool.shutdown();
        logger.debug("Thread pool has been shutdown...");
    }


    private synchronized boolean isStopped() {
        return this.isStopped;
    }

    public synchronized void stop() {
        this.isStopped = true;
        try {
            this.serverSocket.close();
        } catch (IOException e) {
            throw new RuntimeException("Error closing server", e);
        }
    }

    private void openServerSocket() {
        try {
            this.serverSocket = new ServerSocket(this.serverPort);
            logger.debug("Socket server started at port: " + this.serverPort);
        } catch (IOException e) {
            throw new RuntimeException("Cannot open server port", e);
        }
    }
}
