package eu.atos.sla.core;

import eu.project.rapid.common.RapidMessages;
import org.apache.log4j.Logger;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;


public class WorkerRunnable implements Runnable {

    private final static Logger logger = Logger.getLogger(WorkerRunnable.class);

    private Socket clientSocket = null;

    WorkerRunnable(Socket clientSocket) {
        this.clientSocket = clientSocket;
    }

    public void run() {
        try {

            ObjectOutputStream out = new ObjectOutputStream(clientSocket.getOutputStream());
            out.flush();
            ObjectInputStream in = new ObjectInputStream(clientSocket.getInputStream());

            int command = (int) in.readByte();

            long threadId = Thread.currentThread().getId();
            logger.debug("Thread Id: " + threadId + " | Incoming request type: " + command);
            switch (command) {
                case RapidMessages.AC_REGISTER_SLAM:
                    acRegisterSlam(out, in, threadId);
                    break;
                case RapidMessages.VMM_REGISTER_SLAM:
                    vmmRegisterSlam(out, in);
                    break;
            }
            in.close();
            out.close();
            logger.debug("Thread Id: " + threadId + " | Closing socket..");
            clientSocket.close();


        } catch (IOException e) {
            logger.error("Exception", e);
        }
    }

    private void vmmRegisterSlam(ObjectOutputStream out, ObjectInputStream in) throws IOException {
        long threadId = Thread.currentThread().getId();
        try {
            logger.debug("Thread Id: " + threadId + " | VMM_REGISTER_SLAM() start");
            String vmmIP = in.readUTF();
            int vmmPort = in.readInt();
            logger.debug("A vmm has been registered with ip: " + vmmIP + " port: " + vmmPort);
            ThreadPoolServer.vmmIPList.add(vmmIP + "|" + String.valueOf(vmmPort));
            out.writeByte(RapidMessages.OK);
        } catch (Exception e) {
            logger.error("Exception", e);
            out.writeByte(RapidMessages.ERROR);
        }
        logger.debug("Thread Id: " + threadId + " | VMM_REGISTER_SLAM() end");
        out.flush();
    }

    private void acRegisterSlam(ObjectOutputStream out, ObjectInputStream in, long threadId) throws IOException {
        logger.debug("Thread Id: " + threadId + " | AC_REGISTER_SLAM()");
        try {
            //Thread.sleep(2000);
            long userID = in.readLong();
            int osType = in.readInt();
            String vmmIP = in.readUTF();
            int vmmPort = in.readInt();
            int vcpuNum = in.readInt();
            int memSize = in.readInt();
            int gpuCores = in.readInt();
            //TODO **Sokol, qos params will be parsed as last item, next days**

            logger.debug("User id: " + userID + "vmmPort: " + vmmPort +
                    " osType: " + osType + " vmmIP: " + vmmIP +
                    " vcpuNum: " + vcpuNum + " memSize: " + memSize +
                    " gpuCores: " + gpuCores
            );
            String vmIp = slamStartVmVmm(userID, osType, vmmIP, vmmPort,
                    vcpuNum, memSize, gpuCores);
            //String vmIp = "LOCALHOST";
            if (!"".equals(vmIp)) {
                out.writeByte(RapidMessages.OK);
                out.writeUTF(vmIp);
            } else {
                out.writeByte(RapidMessages.ERROR);
            }
            logger.debug("Thread Id: " + threadId + " | Finished processing AC_REGISTER_SLAM()");
        } catch (Exception e) {
            logger.error("Exception", e);
        }
        out.flush();
    }

    private String slamStartVmVmm(long userID, int osType, String vmmIp, int vmmPort,
                                  int vcpuNum, int memSize, int gpuCores
    ) throws IOException {
        logger.debug("slamStartVmVmm() start userID: " + userID + " osType: " + osType);
        logger.debug("Calling VMM manager socket server running at: " + vmmIp + ":" + Integer.toString(vmmPort));
        Socket vmmSocket = new Socket(vmmIp, vmmPort);

        ObjectOutputStream vmmOut = new ObjectOutputStream(vmmSocket.getOutputStream());
        vmmOut.flush();
        ObjectInputStream vmmIn = new ObjectInputStream(vmmSocket.getInputStream());

        vmmOut.writeByte(RapidMessages.SLAM_START_VM_VMM);
        vmmOut.writeLong(userID); // userId
        vmmOut.writeInt(osType);
        vmmOut.writeInt(vcpuNum);
        vmmOut.writeInt(memSize);
        vmmOut.writeInt(gpuCores);
        vmmOut.flush();

        byte status = vmmIn.readByte();
        logger.debug("Return Status: " + (status == RapidMessages.OK ? "OK" : "ERROR"));

        String ip;
        if (status == RapidMessages.OK) {
            long user_id = vmmIn.readLong();//not used
            ip = vmmIn.readUTF();
            logger.debug("Successfully retrieved VM ip: " + ip);
        } else {
            logger.error("Error! returning null..");
            ip = "";
        }
        vmmOut.close();
        vmmIn.close();
        vmmSocket.close();
        logger.debug("SlamStartVmVmm() end");
        return ip;
    }
}